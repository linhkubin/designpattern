﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MoveParabol : MonoBehaviour
{
    public Transform start;
    public Transform finish;
    public Transform tf;


    private void Start()
    {
        StartCoroutine(SimulateParabol(tf, start.position, finish.position, 30, 1, 2, OnDone));
    }

    private void OnDone()
    {
        Debug.Log("move done");
        Invoke("Start",1f);
    }

    IEnumerator SimulateParabol(Transform tf, Vector3 start, Vector3 finish,float firingAngle, float gravity, float speed, UnityAction callBack = null)
    {
        // Short delay added before Projectile is thrown
        yield return new WaitForSeconds(0.05f);

        // Move projectile to the position of throwing object + add some offset if needed.
        tf.position = start;

        // Calculate distance to target
        float target_Distance = Vector3.Distance(start, finish);

        // Calculate the velocity needed to throw the object to the target at specified angle.
        float projectile_Velocity = target_Distance / (Mathf.Sin(2 * firingAngle * Mathf.Deg2Rad) / gravity);

        // Extract the X  Y componenent of the velocity
        float Vx = Mathf.Sqrt(projectile_Velocity) * Mathf.Cos(firingAngle * Mathf.Deg2Rad);
        float Vy = Mathf.Sqrt(projectile_Velocity) * Mathf.Sin(firingAngle * Mathf.Deg2Rad);

        // Calculate flight time.
        float flightDuration = target_Distance / Vx;

        // Rotate projectile to face the target.
        tf.rotation = Quaternion.LookRotation(finish - start);

        float elapse_time = 0;

        while (elapse_time < flightDuration)
        {
            tf.Translate(0, (Vy - (gravity * elapse_time)) * Time.deltaTime * speed, Vx * Time.deltaTime * speed);

            elapse_time += Time.deltaTime * speed;

            yield return null;
        }

        if (callBack != null) callBack.Invoke();
    }
}
