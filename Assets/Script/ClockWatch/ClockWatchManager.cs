﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;

public class ClockWatchManager : MonoBehaviour
{
    public ClockWatchObject cw_prefab;
    private List<ClockWatchObject> list_testObjects = new List<ClockWatchObject>();

    public GameObject g_prefab;
    private List<GameObject> list_g_testObjects = new List<GameObject>();

    const int numberObject = 120000;

    delegate void TransUp();

    TransUp transUp;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < numberObject; i++)
        {
            list_testObjects.Add(Instantiate(cw_prefab));
            //list_g_testObjects.Add(list_testObjects[i].gameObject);
        }

        Invoke("Run",2f);
    }

    private float GetRandom()
    {
        return Random.Range(-10f, 10f);
    }

    private void Run()
    {
        //---------------------------------------------------------1
        Run1();
        //---------------------------------------------------------2
        Run2();
    }


    private void Run1()
    {
        Stopwatch timer1 = new Stopwatch();

        timer1.Start();

        for (int i = 0; i < numberObject; i++)
        {
            list_testObjects[i].SetPos(new Vector3(GetRandom(), GetRandom(), GetRandom()));
            //Instantiate(g_prefab).GetComponent<ClockWatchObject>().SetPos(new Vector3(GetRandom(), GetRandom(), GetRandom()));
        }

        timer1.Stop();

        UnityEngine.Debug.Log("Time Taken1: " + timer1.Elapsed.TotalMilliseconds);

    }

    private void Run2()
    {
        Stopwatch timer2 = new Stopwatch();

        timer2.Start();

        for (int i = 0; i < numberObject; i++)
        {
                (list_testObjects[i] as ObjectChild).SetPos(new Vector3(GetRandom(), GetRandom(), GetRandom()));
        //Instantiate(cw_prefab).SetPos(new Vector3(GetRandom(), GetRandom(), GetRandom()));
    }

        timer2.Stop();

        UnityEngine.Debug.Log("Time Taken2: " + timer2.Elapsed.TotalMilliseconds);

    }
}
