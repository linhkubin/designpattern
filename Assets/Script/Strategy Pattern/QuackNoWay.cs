﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuackNoWay : IQuackBehavior
{
    public void Quack()
    {
        Debug.Log("Can't Quack");
    }

}
