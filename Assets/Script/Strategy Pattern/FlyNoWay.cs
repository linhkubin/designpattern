﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyNoWay : IFlyBehavior
{
    public void Fly()
    {
        Debug.Log("Can't fly");
    }

}
