﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Duck : MonoBehaviour
{
    IFlyBehavior flyBehavior = new FlyNoWay();
    IQuackBehavior quackBehavior = new Quack();

    private void Start()
    {
        flyBehavior.Fly();
        quackBehavior.Quack();
    }

}
